/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}

string translate(string str) {
	string rvalue; //store return value
	string tempword; //store the word that we are reading
	string temp;
	int state = start; //beginning of the state
	map<string,short>::iterator it; //iterate the map for the find function
	updateState(state,str[0]);
	for(size_t i = 0; i < str.length();) {
		switch(state) {
			case scanid:
			while(state == scanid) {
				tempword += str[i]; //all the characters that we read will be stored in the tempword
				i ++;
				updateState(state, str[i]); //increase "i" until we get out of the while loop
			}
			it = hlmap.find(tempword); //check the tempword if it is the keyword in hlmap
			if(it != hlmap.end()) {
				rvalue += hlspans[hlmap[tempword]] + tempword + spanend; //if it is, we going to add it to the return value
			}else {
				rvalue += tempword;
			}
			tempword.clear(); //empty the tempword to store for next word
			break;
			case comment:
			while(i < str.length()) {
				tempword += str[i];
				i++;
				updateState(state,str[i]);
			}
			rvalue += hlspans[hlcomment] + tempword + spanend;
			tempword.clear();
			break;
			case strlit:
			while(state == strlit) {
				tempword += str[i];
				i++;
				updateState(state,str[i]);
			}
			rvalue += hlspans[hlstrlit] + tempword + spanend;
			tempword.clear();
			break;
			case readfs:
			while(state == readfs) {
				tempword += str[i];
				i++;
				updateState(state,str[i]);
			}
			break;
			case readesc:
			while(state == readesc) {
				tempword += str[i];
				i++;
				updateState(state,str[i]);
			}
			rvalue += hlspans[hlescseq] + tempword + spanend;
			tempword.clear();
			break;
			case scannum:
			while(state == scannum) {
				char tempint = str[i];
				tempword += tempint;
				i ++;
				updateState(state,str[i]);
			}
			rvalue += hlspans[hlnumeric] + tempword + spanend;
			tempword.clear();
			break;
			case error:
			while(i < str.length()) {
				tempword += str[i];
				i++;
			}
			rvalue += hlspans[hlerror] + tempword + spanend;
			tempword.clear();
			break;
			case start:
			while(state == start) {
				temp += translateHTMLReserved(str[i]);
				i++;
				updateState(state,str[i]);
				if(i == str.length()) {
					break;
				}
			}
			rvalue += temp;
			temp.clear();
			break;
		}
	}
	return rvalue;
}

int main() {
	// TODO: write the main program.
	// It may be helpful to break this down and write
	// a function that processes a single line, which
	// you repeatedly call from main().
	string line;
	while(getline(cin,line)) {
		cout << translate(line) << "\n";
	}
	return 0;
}
